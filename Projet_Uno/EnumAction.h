#pragma once

namespace EnumAction {

	enum class EnumAction : int {
		PLUS_4,
		PLUS_2,
		PASSE_TOUR,
		CHANGEMENT_SENS,
		JOKER,
		NONE,
	};


	static const EnumAction DependsOnColors[] = { 
		EnumAction::CHANGEMENT_SENS,
		EnumAction::PLUS_2,
		EnumAction::PASSE_TOUR
	};
}