// Projet_Uno.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <vector>
#include "Joueur.h"
#include "JeuCourant.h"
#include "CarteAction.h"
#include "CarteJeu.h"

int main()
{
	vector<Joueur*> vecteur_joueur;

	std::cout << "Bonjour \nSaisissez les informations des joueurs : \n";
	for (size_t i = 1; i <= 3; i++)
	{
		std::string nom_joueur;
		std::string prenom_joueur;
		std::cout << "Joueur " << i << " :\n\t Nom : ";
		std::cin >> nom_joueur;
		std::cout << "\t Prenom : ";
		std::cin >> prenom_joueur;
		Joueur* joueur = new Joueur(nom_joueur, prenom_joueur);
		vecteur_joueur.push_back(joueur);

	}

	JeuCourant jeu_courant = JeuCourant(vecteur_joueur);

	for (auto it : jeu_courant.GetVecteurJoueur()) {
		jeu_courant.DistribCarte(it, jeu_courant.GetVecteurPioche(), 7);
		//it->GetMain().push_back(new CarteAction(EnumCouleur::EnumCouleur::NOIR, EnumAction::EnumAction::JOKER));
		std::cout << "Prenom : " << it->GetPrenom() << " Nom : " << it->GetNom() << " ";
	}
	#ifdef _DEBUG
	for (auto it : jeu_courant.GetVecteurPioche()) {
		it->ToString();
	}
	#endif
	#ifdef _DEBUG
	for (auto it : jeu_courant.GetVecteurJoueur())
	{
		vector<Carte*> main = it->GetMain();

		std::cout << "\n \nMain de " << it->GetPrenom() << " " << it->GetNom() <<" \n" ;
		for (auto it : main) {
			it->ToString();
		}
	}
	#endif
	jeu_courant.StartConsole();
	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
