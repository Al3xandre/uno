#pragma once

namespace EnumCouleur {
	enum class EnumCouleur : int {
		ROUGE,
		BLEU,
		VERT,
		JAUNE,
		NOIR
	};

	static const EnumCouleur Current[] = { EnumCouleur::ROUGE,
		EnumCouleur::BLEU,
		EnumCouleur::VERT,
		EnumCouleur::JAUNE };

}
