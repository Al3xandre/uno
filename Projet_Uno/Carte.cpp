#include "Carte.h"
#include <string>
#include <iostream>
using namespace std;

Carte::Carte(EnumCouleur::EnumCouleur couleur, EnumTypeCarte::EnumTypeCarte type_carte) {
	m_couleur = couleur;
	m_type_carte = type_carte;
}

EnumCouleur::EnumCouleur Carte::GetCouleur()
{
	return m_couleur;
}

void Carte::SetCouleur(EnumCouleur::EnumCouleur couleur)
{
	m_couleur = couleur;
}

EnumTypeCarte::EnumTypeCarte Carte::GetTypeCarte()
{
	return m_type_carte;
}

string Carte::GetValueFromEnum(EnumCouleur::EnumCouleur couleur)
{
	switch (couleur)
	{
	case EnumCouleur::EnumCouleur::ROUGE:
		return "ROUGE";
		break;
	case EnumCouleur::EnumCouleur::BLEU:
		return "BLEU";
		break;
	case EnumCouleur::EnumCouleur::VERT:
		return "VERT";
		break;
	case EnumCouleur::EnumCouleur::JAUNE:
		return "JAUNE";
		break;
	case EnumCouleur::EnumCouleur::NOIR:
		return "NOIR";
		break;
	default:
		return "unknown color";
		break;
	}
}

