#include "Joueur.h"
#include "Carte.h"
#include <string>
using namespace std;

Joueur::Joueur(string nom, string prenom) {
	m_nom = nom;
	m_prenom = prenom;
	m_vecteur_main = vector<Carte*>();
}

void Joueur::SetMain(vector<Carte*> vecteur_carte)
{
	m_vecteur_main = vecteur_carte;
}

string Joueur::GetPrenom()
{
	return m_prenom;
}

string Joueur::GetNom()
{
	return m_nom;
}

vector<Carte*>& Joueur::GetMain()
{
	return m_vecteur_main;
}
