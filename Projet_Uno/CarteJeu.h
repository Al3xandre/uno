#pragma once

#include "Carte.h"

#include <string>
using namespace std;

class CarteJeu : public Carte {
private: int m_nombre;

public:
	CarteJeu(EnumCouleur::EnumCouleur couleur, int nombre);
	EnumAction::EnumAction GetAction() override;
	int GetNombre() override;
	void ToString() override;
	string MakeString() override;
};