#pragma once

#include "EnumCouleur.h"
#include "EnumAction.h"
#include "EnumTypeCarte.h"
#include <string>
using namespace std;

class Carte {

private: EnumCouleur::EnumCouleur m_couleur;
	   EnumTypeCarte::EnumTypeCarte m_type_carte;
protected:
	Carte(EnumCouleur::EnumCouleur, EnumTypeCarte::EnumTypeCarte);
public:
	EnumCouleur::EnumCouleur GetCouleur();
	void SetCouleur(EnumCouleur::EnumCouleur);
	virtual EnumAction::EnumAction GetAction() = 0;
	EnumTypeCarte::EnumTypeCarte GetTypeCarte();
	virtual int GetNombre() = 0;
	virtual void ToString() = 0;
	virtual string MakeString() = 0;
	static string GetValueFromEnum(EnumCouleur::EnumCouleur);
};