#include "Regle.h"
#include <iostream>
#include "EnumCouleur.h"
using namespace std;

bool Regle::CanPutDownCard(Carte* carte_a_joue, Carte* carte_precedente)
{
	if (carte_a_joue->GetCouleur() == EnumCouleur::EnumCouleur::NOIR) {
		return true;
	}
	else if (carte_precedente->GetCouleur() == carte_a_joue->GetCouleur()) {
		return true;
	}
	else {
		switch (carte_a_joue->GetTypeCarte())
		{
		case EnumTypeCarte::EnumTypeCarte::ACTION:
			if (carte_precedente->GetTypeCarte() == EnumTypeCarte::EnumTypeCarte::ACTION) {
				return carte_precedente->GetAction() == carte_a_joue->GetAction();
			}
			return false;
			break;
		case EnumTypeCarte::EnumTypeCarte::JEU:
			if (carte_precedente->GetTypeCarte() == EnumTypeCarte::EnumTypeCarte::JEU) {
				return carte_precedente->GetNombre() == carte_a_joue->GetNombre();
			}
			return false;
			break;
		default:
			cerr << "ce type de carte n'existe pas.";
			return true;
			break;
		}
	}

}

void Regle::Action(JeuCourant* jeu_courant)
{
	switch (jeu_courant->GetVecteurCarteJoue().back()->GetAction())
	{
	case EnumAction::EnumAction::CHANGEMENT_SENS:
		jeu_courant->SetSensJeu(!jeu_courant->GetSensJeu());
		break;
	case EnumAction::EnumAction::JOKER:
		ChangeColor(jeu_courant);
		break;
	case EnumAction::EnumAction::PASSE_TOUR:
		jeu_courant->DealWithPlayer();
		break;
	case EnumAction::EnumAction::PLUS_4:
	{
		ChangeColor(jeu_courant);
		Joueur* next_joueur = jeu_courant->GetNextJoueur();
		jeu_courant->DistribCarte(next_joueur, jeu_courant->GetVecteurPioche(), 4);
	}
	break;
	case EnumAction::EnumAction::PLUS_2:
	{
		Joueur* next_joueur = jeu_courant->GetNextJoueur();
		jeu_courant->DistribCarte(next_joueur, jeu_courant->GetVecteurPioche(), 2);
	}
	break;
	default:
		break;
	}
}

void Regle::ChangeColor(JeuCourant* jeu_courant)
{
	int couleur_index;
	do {
		cout << "choisissez la couleur: \n";
		int index = 0;
		for (auto it : EnumCouleur::Current) {
			cout << index << " - " << Carte::GetValueFromEnum(it) << "\n";
			index++;
		}
		cin >> couleur_index;
		Carte* carte = jeu_courant->GetVecteurCarteJoue().back();
		carte->SetCouleur(EnumCouleur::Current[couleur_index]);
	} while (couleur_index < 0 || couleur_index > 3);
}


