#pragma once

#include "Carte.h"
#include "EnumAction.h"

class CarteAction : public Carte {
private: EnumAction::EnumAction m_action;

public:
	CarteAction(EnumCouleur::EnumCouleur, EnumAction::EnumAction);
	EnumAction::EnumAction GetAction() override;
	int GetNombre() override;
	string GetValueFromEnum(EnumAction::EnumAction);
	void ToString() override;
	string MakeString() override;

};