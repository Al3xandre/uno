#include "CarteJeu.h"
#include <iostream>
#include <sstream>

CarteJeu::CarteJeu(EnumCouleur::EnumCouleur couleur, int nombre) :Carte(couleur, EnumTypeCarte::EnumTypeCarte::JEU) {
	m_nombre = nombre;
}

EnumAction::EnumAction CarteJeu::GetAction()
{
	return EnumAction::EnumAction::NONE;
}

int CarteJeu::GetNombre()
{
	return m_nombre;
}

void CarteJeu::ToString() {
	std::cout << "\n - La couleur est " << Carte::GetValueFromEnum(Carte::GetCouleur()) << " le nombre est " << GetNombre() << "\n";
}

string CarteJeu::MakeString() {
	std::stringstream s;
	s << "La couleur est " << Carte::GetValueFromEnum(Carte::GetCouleur()) << " le nombre est " << GetNombre() << "\n";
	return s.str();
}