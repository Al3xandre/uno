#include "JeuCourant.h"
#include "CarteJeu.h"
#include "CarteAction.h"
#include "Regle.h"
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;


JeuCourant::JeuCourant(vector<Joueur*> joueurs) {
	m_vecteur_carte_pioche = JeuCourant::init_jeu();
	m_vecteur_carte_joue = vector<Carte*>();
	m_vecteur_joueur = joueurs;
}

vector<Carte*> JeuCourant::init_jeu() {
	vector<Carte*> vecteur_pioche = vector<Carte*>();
	m_index_joueur = 0;
	for (auto it : EnumCouleur::Current) {
		for (unsigned int index = 1; index <= 9; index++) {
			CarteJeu* carte = new CarteJeu(it, index);
			vecteur_pioche.push_back(carte);
		}
		for (unsigned int index = 0; index <= 9; index++) {
			CarteJeu* carte = new CarteJeu(it, index);
			vecteur_pioche.push_back(carte);
		}
		for (auto itAction : EnumAction::DependsOnColors) {
			CarteAction* carte = new CarteAction(it, itAction);
			vecteur_pioche.push_back(carte);
			vecteur_pioche.push_back(carte);
		}
	}
	CarteAction* carte_plus_4 = new CarteAction(EnumCouleur::EnumCouleur::NOIR, EnumAction::EnumAction::PLUS_4);
	vecteur_pioche.push_back(carte_plus_4);
	vecteur_pioche.push_back(carte_plus_4);
	vecteur_pioche.push_back(carte_plus_4);
	vecteur_pioche.push_back(carte_plus_4);
	CarteAction* carte_joker = new CarteAction(EnumCouleur::EnumCouleur::NOIR, EnumAction::EnumAction::JOKER);
	vecteur_pioche.push_back(carte_joker);
	vecteur_pioche.push_back(carte_joker);
	vecteur_pioche.push_back(carte_joker);
	vecteur_pioche.push_back(carte_joker);
	return vecteur_pioche;
}

vector<Carte*>& JeuCourant::GetVecteurPioche()
{
	return m_vecteur_carte_pioche;
}

vector<Carte*>& JeuCourant::GetVecteurCarteJoue()
{
	return m_vecteur_carte_joue;
}

vector<Joueur*>& JeuCourant::GetVecteurJoueur()
{
	return m_vecteur_joueur;
}

Joueur* JeuCourant::GetNextJoueur()
{
	if (m_sens_de_jeu)
	{
		if (m_index_joueur != m_vecteur_joueur.size() - 1)
		{
			return m_vecteur_joueur.at(m_index_joueur+1);
		}
		else {
			return  m_vecteur_joueur.front();
		}
	}
	else
	{
		if (m_index_joueur != 0)
		{
			return m_vecteur_joueur.at(m_index_joueur-1);
		}
		else
		{
			return m_vecteur_joueur.back();
		}
	}
}

void JeuCourant::SetSensJeu(bool sens_jeu)
{
	m_sens_de_jeu = sens_jeu;
}

bool JeuCourant::GetSensJeu()
{
	return m_sens_de_jeu;
}

void JeuCourant::StartConsole()
{

	if (m_vecteur_joueur.size() == 0)
		return;
	bool partie_ended = false;
	m_sens_de_jeu = true;
	m_joueur_en_cours = m_vecteur_joueur[0];

#pragma region Premiere carte retourn�e

	srand((unsigned int)time(NULL));
	int randomIndex = rand() % m_vecteur_carte_pioche.size();
	m_vecteur_carte_joue.push_back(m_vecteur_carte_pioche.at(randomIndex)); // ajout de la carte dans la pile de jeu
	m_vecteur_carte_pioche.erase(m_vecteur_carte_pioche.begin() + randomIndex); //suppression de la carte de la pioche

#pragma endregion


		Regle::Action(this);
	do
	{
		cout << "\nLa derniere carte jouee est " << m_vecteur_carte_joue.back()->MakeString();
		cout << "\n C'est au tour du joueur " << m_joueur_en_cours->GetPrenom() << " de jouer.";
		system("pause");

		cout << "\n La main du joueur " << m_joueur_en_cours->GetPrenom() << " est : \n";
		for (unsigned int i = 0; i < m_joueur_en_cours->GetMain().size(); i++) {
			cout << i << " : " << m_joueur_en_cours->GetMain().at(i)->MakeString();
		}

		int action;

		if (m_deja_pioche) {
			do
			{
				cout << "Quelle action voulez - vous faire ? \n \t - 1 - Passer son tour \n \t 1 - Poser une carte\n \t";
				cin >> action;
			} while (action != -1 && action != 1);
			m_deja_pioche = false;
		}
		else {
			do
			{
				cout << "Quelle action voulez-vous faire ? \n \t 1 - Poser une carte\n \t 2 - Piocher une carte \n";
				cin >> action;
			} while (action != 1 && action != 2);
		}


		if (action == 1)
		{
#pragma region poser une carte


			int carte_index_main;
			do
			{
				cout << "Quelle carte voulez-vous poser ";
				cin >> carte_index_main;
			} while (carte_index_main < 0 || carte_index_main > m_joueur_en_cours->GetMain().size() - 1);

#pragma region carte jou� ou pioche


				Carte* carte_a_joue = m_joueur_en_cours->GetMain().at(carte_index_main);
				Carte* carte_precedente = m_vecteur_carte_joue.back();

				if (Regle::CanPutDownCard(carte_a_joue, carte_precedente) == true) {
					cout << "la carte jou� est " << carte_a_joue->MakeString();
					m_vecteur_carte_joue.push_back(m_joueur_en_cours->GetMain().at(carte_index_main)); // ajout de la carte dans la pile de jeu
					m_joueur_en_cours->GetMain().erase(m_joueur_en_cours->GetMain().begin() + carte_index_main); //suppression de la carte dans la main
					Regle::Action(this);
					if (m_joueur_en_cours->GetMain().size() == 0)
					{
						partie_ended = true;
					}

					if (partie_ended != true)
					{
#pragma region gestion du sens du jeu
						DealWithPlayer();
#pragma endregion
					}
				}
				else {
					cout << "Cette carte ne peut pas etre jouee";
				}
#pragma endregion

#pragma endregion
		}
		else if (action == 2)
		{
#pragma region Pioche
			DistribCarte(m_joueur_en_cours, m_vecteur_carte_pioche, 1);
			m_deja_pioche = true;
#pragma endregion
		}
		else if (action == -1) {
#pragma region Passe le tour
			DealWithPlayer();
#pragma endregion
		}




		else
		{
			cout << "\nC'est win !";
		}


	} while (partie_ended != true);
}

void JeuCourant::DealWithPlayer()
{
	if (m_sens_de_jeu)
	{
		if (m_index_joueur != m_vecteur_joueur.size() - 1)
		{
			m_joueur_en_cours = m_vecteur_joueur.at(++m_index_joueur);
		}
		else {
			m_joueur_en_cours = m_vecteur_joueur.front();
			m_index_joueur = 0;
		}
	}
	else
	{
		if (m_index_joueur != 0)
		{
			m_joueur_en_cours = m_vecteur_joueur.at(--m_index_joueur);
		}
		else
		{
			m_joueur_en_cours = m_vecteur_joueur.back();
			m_index_joueur = (int)m_vecteur_joueur.size()- 1;
		}
	}
}

void JeuCourant::DistribCarte(Joueur* joueur, vector<Carte*>& vecteur_carte, int nb_carte_max)
{
	int nb_carte = 0;
	vector<Carte*> main = joueur->GetMain();
	while (nb_carte < nb_carte_max)
	{
		srand((unsigned int)time(NULL));
		int randomIndex = rand() % vecteur_carte.size();
		main.push_back(vecteur_carte[randomIndex]);
		vecteur_carte.erase(vecteur_carte.begin() + randomIndex);
		nb_carte++;
	}
	joueur->SetMain(main);
}