#pragma once

#include "Joueur.h"

using namespace std;

class JeuCourant {
private:
	vector<Carte*> m_vecteur_carte_pioche;
	vector<Carte*> m_vecteur_carte_joue;
	vector<Joueur*> m_vecteur_joueur;
	bool m_sens_de_jeu; /// true = sens horaire ; false = sens anti-horaire
	bool m_deja_pioche;
	Joueur* m_joueur_en_cours;
	int m_index_joueur;

public:
	JeuCourant(vector<Joueur*>);
	vector<Carte*> init_jeu();
	vector<Carte*>& GetVecteurPioche();
	vector<Carte*>& GetVecteurCarteJoue();
	vector<Joueur*>& GetVecteurJoueur();
	Joueur* GetNextJoueur();
	void SetSensJeu(bool);
	bool GetSensJeu();
	void DistribCarte(Joueur*, vector<Carte*>&, int);
	void StartConsole();
	void DealWithPlayer();
};