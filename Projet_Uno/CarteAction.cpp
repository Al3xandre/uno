#include "CarteAction.h"
#include <iostream>
#include <sstream>
CarteAction::CarteAction(EnumCouleur::EnumCouleur couleur, EnumAction::EnumAction action) : Carte(couleur, EnumTypeCarte::EnumTypeCarte::ACTION)
{
	m_action = action;
}

EnumAction::EnumAction CarteAction::GetAction()
{
	return m_action;
}

int CarteAction::GetNombre()
{
	return -1;
}

string CarteAction::GetValueFromEnum(EnumAction::EnumAction action)
{
	switch (action)
	{
	case EnumAction::EnumAction::PLUS_4:
		return "PLUS_4";
		break;
	case EnumAction::EnumAction::PLUS_2:
		return "PLUS_2";
		break;
	case EnumAction::EnumAction::PASSE_TOUR:
		return "PASSE_TOUR";
		break;
	case EnumAction::EnumAction::CHANGEMENT_SENS:
		return "CHANGEMENT_SENS";
		break;
	case EnumAction::EnumAction::JOKER:
		return "JOKER";
		break;
	default:
		return "unknown action";
		break;
	}
}

void CarteAction::ToString() {
	std::cout << "\n - La couleur est " << Carte::GetValueFromEnum(Carte::GetCouleur()) << " l'action est " << GetValueFromEnum(GetAction()) << "\n";
}
string CarteAction::MakeString() {
	std::stringstream s;
	s << "La couleur est " << Carte::GetValueFromEnum(Carte::GetCouleur()) << " l'action est " << GetValueFromEnum(GetAction())<<"\n";
	return s.str();
}