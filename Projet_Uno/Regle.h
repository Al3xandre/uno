#pragma once
#include "Carte.h"
#include "JeuCourant.h"

class Regle {
public:
	static bool CanPutDownCard(Carte*, Carte*);
	static void Action(JeuCourant*);
	static void ChangeColor(JeuCourant*);
};