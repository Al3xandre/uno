#pragma once
#include <string>
#include <vector>
#include "Carte.h"
using namespace std;

class Joueur {

#pragma region Attributes

private:
	string m_nom;
	string m_prenom;
	vector<Carte*> m_vecteur_main;

#pragma endregion

public:
	Joueur(string, string);
	void SetMain(vector<Carte*>);
	string GetPrenom();
	string GetNom();
	vector<Carte*>& GetMain();
};